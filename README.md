```plantuml
@startmindmap
<Style>
mindmapDiagram{
.node{
BackgroundColor pink
HorizontalAlignment center
  }
  .sep{
      BackgroundColor skyblue
  }
}
</style>
*[#yellow] Industria 4.0 <<root>>
**[#orange] ¿En que consiste?
*** Se caracteriza por un sistema\n de producción inteligente\n con decisión autónoma
**** Es decir <<sep>>
***** La idea es que las máquinas vayan\n tomando sus propias decisiones\n en el proceso de producción y que\n exista cada vez menos intervención humana
--[#brown] El término se utiliza como
--- El proceso por el cual las empresas\n y la sociedad organizan sus métodos\n de trabajo y estrategias para obtener mas beneficios\n gracias a la implementación de las nuevas tegnologías
**[#limegreen] Cambios que genera en nuestras vidas\n y en las formas de hacer los negocios
*** Cambio 1 <<sep>>
****  Integración de TICs en la industria\n de manufactura y servicios
***** Por ejemplo <<sep>>
****** Ya casi no existe la participación\n de los seres humanos
******* Esto es <<sep>>
******** gracias a la aparición de\n nuevas tecnologías
**** Efecto 1 <<sep>>
***** reducción de los puestos de trabajo
****** ¿Qué tipo de trabajos? <<sep>>
******* La mayoría de los trabajos\n que requeiren procesos repetitivos\n o de decisiones basadas en datos\n históricos serán remplazados por robots\n o computadoras con inteligencia artificial
**** Efecto 2 <<sep>>
***** Aparición de nuevas profesiones
****** Esto es <<sep>>
******* gracias a las nuevas tecnologías y debido\n a ellas han aparecido nuevas formas\n de generar dinero
*** Cambio 2 <<sep>>
**** Transformación de las empresas de\n manufactura en empresas de TICs
***** Esto es por ejemplo <<sep>>
****** Que ahora la mayoría de las cosas\n que utilizamos cuentan con un sitema\n operativo, principalmente los electrodomésticos
**** Efecto <<sep>>
***** Cada vez existe menos diferenciación entre las industrias
*** Cambio 3 <<sep>>
**** Nuevos paradigmas y tecnologías
***** Primer paradigma <<sep>>
****** La velocidad, pues no es el pez mas\n grande el que se come a los demás\n sino el pez mas rápido.
******* Es decir <<sep>>
******** Si las empresas no se adaptan a\n los nuevos cambios tecnológicos,\n estas están destinadas a desaparecer
***** Otro paradigma <<sep>>
****** Negocios basados en plataformas

@endmindmap
```
